;;; -*- SIMPLE-BIT-VECTOR low level routines and convenient eDSL over it. -*-

(defpackage #:bitfield-schema-asd
  (:use :cl :asdf))

(in-package #:bitfield-schema-asd)

(defsystem bitfield-schema
  :name "bitfield-schema"
  :version "0.1"
  :author "swizard"
  :licence "BSD"
  :description "SIMPLE-BIT-VECTOR low level routines and convenient eDSL over it."
  :depends-on (:iterate)
  :components ((:file "package")
               (:file "routines" :depends-on ("package"))
               (:file "optimize" :depends-on ("package"))
               (:file "dsl" :depends-on ("routines" "optimize"))))
